﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Skeleton {

    // Static information
    private const float minDepthKinect = 7000f;
    private const float maxDepthKinect = 15000f;
    private const float minDepthWorld = 0f;
    private const float maxDepthWorld = 10f;

    private const float CYLINDER_HEIGHT = 2f;

    private static float depthFactor = (maxDepthWorld - minDepthWorld) / (maxDepthKinect - minDepthKinect);

    private int SkeletonBoneLayer;

    // data
    private KinectManager kinect;
    private JointType[,] bones;
    private GameObject[] bonesObjects;

    public Skeleton(KinectManager kinect)
    {
        SkeletonBoneLayer = LayerMask.NameToLayer("SkeletonBone");

        this.kinect = kinect;
        
        initializeBones();
       


    }

    private void initializeBones()
    {
        this.bones = new JointType[,] { 
            // Left Arm
            {JointType.HandLeft, JointType.WristLeft },
            {JointType.WristLeft, JointType.ElbowLeft },
            {JointType.ElbowLeft, JointType.ShoulderLeft },
            {JointType.ShoulderLeft, JointType.SpineShoulder },

            //{JointType.HandLeft, JointType.ElbowLeft },
            //{JointType.HandLeft, JointType.ElbowLeft },
            //{JointType.HandLeft, JointType.ElbowLeft },
            //{JointType.HandLeft, JointType.ElbowLeft },
            //{JointType.HandLeft, JointType.ElbowLeft },
            //{JointType.HandLeft, JointType.ElbowLeft }
        };

        this.bonesObjects = new GameObject[this.bones.Length / 2];
        for (int i = 0; i < this.bonesObjects.Length; i++)
        {
            this.bonesObjects[i] = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            this.bonesObjects[i].layer = SkeletonBoneLayer;
        }
    }

    public void update()
    {
        updateBones();
    }

    private void updateBones()
    {
        for (int i = 0; i < this.bonesObjects.Length; i++)
        {
            updateBone(i);
        }
    }

    private void updateBone(int index)
    {
        int joint1 = (int)this.bones[index, 0];
        int joint2 = (int)this.bones[index, 1];
        GameObject worldBone = this.bonesObjects[index];

        uint playerID = kinect.GetPlayer1ID();

        if(kinect.IsJointTracked(playerID,joint1) && kinect.IsJointTracked(playerID, joint2))
        {
            // Joint 1
            Vector3 j1kp = kinect.GetRawSkeletonJointPos(playerID, joint1);
            Vector3 jPos1 = jointPositionToWorldPosition(j1kp);

            // Joint 2
            Vector3 j2kp = kinect.GetRawSkeletonJointPos(playerID, joint2);
            Vector3 jPos2 = jointPositionToWorldPosition(j2kp);

            // Update Gameobject
            worldBone.SetActive(true);

            Vector3 newPosition = (jPos1 + jPos2) / 2;
            worldBone.transform.position = newPosition;
            worldBone.transform.LookAt(jPos1);

            worldBone.transform.Rotate(new Vector3(90, 0, 0));

            float distance = Vector3.Distance(jPos1, jPos2);
            worldBone.transform.localScale = new Vector3(worldBone.transform.localScale.x, (distance / 2), worldBone.transform.localScale.z);
        }
        else
        {
            worldBone.SetActive(false);
        }
         

    }

    private Vector3 jointPositionToWorldPosition(Vector3 jPos)
    {
        Vector2 posDepth = kinect.GetDepthMapPosForJointPos(jPos);
        Vector2 posColor = kinect.GetColorMapPosForDepthPos(posDepth);

        ushort depth = kinect.GetDepthForPixel((int)posColor.x, (int)posColor.y);
        if (depth < minDepthKinect) depth = (ushort)minDepthKinect;
        if (depth > maxDepthKinect) depth = (ushort)maxDepthKinect;

        float worldDepth = (depth - minDepthKinect) * depthFactor + minDepthWorld;

        float scaleX = (float)posColor.x / KinectWrapper.Constants.ColorImageWidth;
        float scaleY = 1.0f - (float)posColor.y / KinectWrapper.Constants.ColorImageHeight;

        Vector3 vPos = Camera.main.ViewportToWorldPoint(new Vector3(scaleX, scaleY, 10));
        Vector3 worldPos = new Vector3(vPos.x, vPos.y, worldDepth);

        return worldPos;
    }
}
