﻿using UnityEngine;
using System.Collections;

public class CameraDrawer : MonoBehaviour
{

    public GUITexture gTexture;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        KinectManager manager = KinectManager.Instance;
        if (manager && manager.IsInitialized())
        {
            // Get kinect image
            gTexture.texture = manager.GetUsersLblTex();

        }
    }

}
