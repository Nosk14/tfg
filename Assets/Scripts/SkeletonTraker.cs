﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class SkeletonTraker : MonoBehaviour
{

    //private const float minDepthKinect = 7000f;
    //private const float maxDepthKinect = 15000f;
    //private const float minDepthWorld = 0f;
    //private const float maxDepthWorld = 10f;

    //private const float CYLINDER_HEIGHT = 2f;

    //private static float depthFactor = (maxDepthWorld - minDepthWorld) / (maxDepthKinect - minDepthKinect);

    private Skeleton skeleton;

    // Use this for initialization
    void Start()
    {
        this.skeleton = null;
    }

    // Update is called once per frame
    void Update()
    {
        KinectManager manager = KinectManager.Instance;
        if (manager && manager.IsInitialized() && manager.IsUserDetected())
        {
            if(this.skeleton == null)
            {
                this.skeleton = new Skeleton(manager);
            }
            skeleton.update();

            //uint userId = manager.GetPlayer1ID();

            //if (manager.IsJointTracked(userId, (int)JointType.HandLeft))
            //{
            //    Vector3 jPos = manager.GetRawSkeletonJointPos(userId, (int)JointType.HandLeft);

            //    Vector2 posDepth = manager.GetDepthMapPosForJointPos(jPos);
            //    Vector2 posColor = manager.GetColorMapPosForDepthPos(posDepth);


            //    ushort depth = manager.GetDepthForPixel((int)posColor.x, (int)posColor.y);
            //    if (depth < minDepthKinect) depth = (ushort)minDepthKinect;
            //    if (depth > maxDepthKinect) depth = (ushort)maxDepthKinect;

            //    float worldDepth = (depth - minDepthKinect) * depthFactor + minDepthWorld;

            //    float scaleX = (float)posColor.x / KinectWrapper.Constants.ColorImageWidth;
            //    float scaleY = 1.0f - (float)posColor.y / KinectWrapper.Constants.ColorImageHeight;

            //    Vector3 vPos = Camera.main.ViewportToWorldPoint(new Vector3(scaleX, scaleY, 10));
            //    Vector3 worldPos = new Vector3(vPos.x, vPos.y, worldDepth);
            //    obj.transform.position = worldPos;

            //}

        }
    }
}
