﻿using UnityEngine;
using System.Collections;

public class CreateObject : MonoBehaviour
{

    public Transform p1;
    public Transform p2;
    private GameObject cylinder;

    // Use this for initialization
    void Start()
    {
        cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        Skeleton sk = new Skeleton(null);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 spawn = (p1.position + p2.position) / 2;
        cylinder.transform.position = spawn;
        cylinder.transform.LookAt(p1);

        cylinder.transform.Rotate(new Vector3(90, 0, 0));

        float distance = Vector3.Distance(p1.position, p2.position);
        cylinder.transform.localScale = new Vector3(cylinder.transform.localScale.x, (distance / 2), cylinder.transform.localScale.z);
    }
}
